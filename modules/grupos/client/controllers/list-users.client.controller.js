'use strict';

angular.module('grupos').controller('UserListGroupController', ['$scope', '$filter', 'Admin', '$state',
  function ($scope, $filter, Admin, $state) {
      Admin.query(function (data) {
          if($scope.userList && $scope.userList.length > 0){
              $scope.groupUsers = $scope.pruneUsersList($scope.userList, data);
              data = $scope.pruneOtherUsersList($scope.userList, data);
          }else{
              $scope.groupUsers = [];
          }
          $scope.users = data;

          $scope.buildPager();
      });

      $scope.buildPager = function () {
      $scope.pagedItems = [];
      $scope.itemsPerPage = 15;
      $scope.currentPage = 1;
      $scope.figureOutItemsToDisplay();
    };

    $scope.figureOutItemsToDisplay = function () {
      $scope.filteredItems = $filter('filter')($scope.users, {
        $: $scope.search
      });
      $scope.filterLength = $scope.filteredItems.length;
      var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
      var end = begin + $scope.itemsPerPage;
      $scope.pagedItems = $scope.filteredItems.slice(begin, end);
    };

    $scope.pageChanged = function () {
      $scope.figureOutItemsToDisplay();
    };

    $scope.pruneUsersList = function (usersList, data) {
        var currentUsers = [];
        // var deleteUsers = [];
        data.forEach(function (currentData) {
            if(usersList.indexOf(currentData._id) !== -1){
                currentUsers.push(currentData);
            }
            // usersList.forEach(function (user) {
            //     if((deleteUsers.length == 0 || deleteUsers.indexOf(user) === -1) && user === currentData._id){
            //         currentUsers.push(currentData);
            //     }else{
            //         deleteUsers.push(user);
            //     }
            // });
        });
        return currentUsers;
    };

      $scope.pruneOtherUsersList = function (usersList, data) {
          var otherUsers = [];
          data.forEach(function (currentData) {
              if(usersList.indexOf(currentData._id) === -1){
                  otherUsers.push(currentData);
              }
          });
          // var deleteUsers = [];
          // data.forEach(function (currentData) {
          //     usersList.forEach(function (user) {
          //         if((deleteUsers.length == 0 ||deleteUsers.indexOf(user) === -1) && user !== currentData._id){
          //             otherUsers.push(currentData);
          //         }else{
          //             deleteUsers.push(currentData);
          //         }
          //     });
          //     // if(insert){
          //     //     otherUsers.push(currentData);
          //     // }
          // });
          return otherUsers;
      };

    $scope.addUser = function(user){
        console.debug(user);
        $scope.groupUsers.push(user);
        $scope.pagedItems.splice($scope.pagedItems.indexOf(user), 1);
    };

    $scope.removeUser = function(user){
        console.debug(user);
        $scope.pagedItems.push(user);
        $scope.groupUsers.splice($scope.groupUsers.indexOf(user), 1);
    };
  }
]);
