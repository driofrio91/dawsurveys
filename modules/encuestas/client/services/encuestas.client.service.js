// Encuestas service used to communicate Encuestas REST endpoints
(function () {
  'use strict';

  angular
    .module('encuestas')
    .factory('EncuestasService', EncuestasService);

  EncuestasService.$inject = ['$resource'];

  function EncuestasService($resource) {
    return $resource('api/encuestas/:encuestaId', {
      encuestaId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
}());
