// Grupos service used to communicate Grupos REST endpoints
(function () {
    'use strict';

    angular
        .module('grupos')
        .factory('GrupoService', GrupoService);

    GrupoService.$inject = ['$resource'];

    function GrupoService($resource) {
        return $resource('api/grupos/:grupoId', {
            grupoId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
}());
