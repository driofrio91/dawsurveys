(function () {
  'use strict';

  // Grupos controller
  angular
    .module('grupos')
    .controller('GruposController', GruposController);

  GruposController.$inject = ['$scope', '$state', '$window', 'Authentication', 'grupoResolve'];

  function GruposController ($scope, $state, $window, Authentication, grupo) {
    var vm = this;

    vm.authentication = Authentication;
    vm.grupo = grupo;
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;

    if(vm.grupo.groupUsers){
        $scope.userList = angular.copy(vm.grupo.groupUsers);
    }

    // Remove existing Grupo
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.grupo.$remove($state.go('grupos.list'));
      }
    }

    // Save Grupo
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.grupoForm');
        return false;
      }
      //Se añaden los usuarios al grupo
      addUsers();

      // TODO: move create/update logic to service
      if (vm.grupo._id) {
        vm.grupo.$update(successCallback, errorCallback);
      } else {
        vm.grupo.$save(successCallback, errorCallback);
      }

      function successCallback(res) {
        $state.go('grupos.view', {
          grupoId: res._id
        });
      }

      function errorCallback(res) {
        vm.error = res.data.message;
      }
    }

    function addUsers() {
        //Se comprueba que exista la lista de usuarios
        if(!vm.grupo.groupUsers){
            vm.grupo.groupUsers = [];
        }

        if(vm.grupo){
            if($scope.groupUsers.length > 0){
                vm.grupo.groupUsers = [];
                $scope.groupUsers.forEach(function (groupUser) {
                    // if(vm.grupo.groupUsers.length > 0){
                    //     vm.grupo.groupUsers.forEach(function (currentGroupUser) {
                    //         if(groupUser._id !== currentGroupUser){
                    //             vm.grupo.groupUsers.push(groupUser._id);
                    //         }
                    //     });
                    // }else{
                    //     vm.grupo.groupUsers.push(groupUser._id);
                    // }
                    vm.grupo.groupUsers.push(groupUser._id);
                });
            }else{
                vm.grupo.groupUsers = [];
            }
        }
    }
  }
}());
