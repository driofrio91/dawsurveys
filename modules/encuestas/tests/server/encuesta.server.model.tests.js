'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Encuesta = mongoose.model('Encuesta');

/**
 * Globals
 */
var user,
  encuesta;

/**
 * Unit tests
 */
describe('Encuesta Model Unit Tests:', function() {
  beforeEach(function(done) {
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: 'username',
      password: 'password'
    });

    user.save(function() {
      encuesta = new Encuesta({
        name: 'Encuesta Name',
        user: user
      });

      done();
    });
  });

  describe('Method Save', function() {
    it('should be able to save without problems', function(done) {
      this.timeout(0);
      return encuesta.save(function(err) {
        should.not.exist(err);
        done();
      });
    });

    it('should be able to show an error when try to save without name', function(done) {
      encuesta.name = '';

      return encuesta.save(function(err) {
        should.exist(err);
        done();
      });
    });
  });

  afterEach(function(done) {
    Encuesta.remove().exec(function() {
      User.remove().exec(function() {
        done();
      });
    });
  });
});
