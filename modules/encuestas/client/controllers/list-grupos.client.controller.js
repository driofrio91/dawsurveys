(function () {
  'use strict';

  angular
    .module('grupos')
    .controller('GruposListControllerEncuestas', GruposListControllerEncuestas);

    GruposListControllerEncuestas.$inject = ['GruposService', '$scope', '$filter', 'Admin'];

  function GruposListControllerEncuestas(GruposService, $scope, $filter, Admin) {
    $scope.groups = this;

    $scope.groups = GruposService.query(function (data) {
        $scope.group = data;
        $scope.buildPager();
    });

    $scope.buildPager = function () {
        $scope.pagedItems = [];
        $scope.itemsPerPage = 15;
        $scope.currentPage = 1;
        $scope.figureOutItemsToDisplay();
    };

    $scope.figureOutItemsToDisplay = function () {
        $scope.filteredItems = $filter('filter')($scope.group, {
            $: $scope.search
        });
        $scope.filterLength = $scope.filteredItems.length;
        var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
        var end = begin + $scope.itemsPerPage;
        $scope.pagedItems = $scope.filteredItems.slice(begin, end);
      };

    $scope.pageChanged = function () {
        $scope.figureOutItemsToDisplay();
    };

    $scope.selectGroup = function (group) {
        $scope.groupSelected = group;
    };
    $scope.deleteGroup = function () {
        $scope.groupSelected = null;
    };

  }
}());
