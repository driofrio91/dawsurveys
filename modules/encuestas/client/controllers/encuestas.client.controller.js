(function () {
  'use strict';

  // Encuestas controller
  angular
    .module('encuestas')
    .controller('EncuestasController', EncuestasController);

  EncuestasController.$inject = ['$scope', '$state', '$window', 'Authentication', 'encuestaResolve', 'GrupoService'];

  function EncuestasController ($scope, $state, $window, Authentication, encuesta, GrupoService) {
    var vm = this;

    vm.authentication = Authentication;
    vm.encuesta = encuesta;
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;
    vm.addQuestion = addQuestion;
    vm.modifyAssessment = modifyAssessment;
    /*new question*/
    vm.newQuestion = null;
    /*Lista de preguntas*/
    if(!vm.encuesta.questions){
        vm.encuesta.questions = [];
    }
    /*Grupo*/
    if(vm.encuesta.group){
        $scope.groupSelected = getGroup(vm.encuesta.group, GrupoService);
    }
console.log($scope.groupSelected);
    function modifyAssessment(item , assessment) {
        vm.encuesta.questions.forEach(function(currentQuestion){
          if(currentQuestion === item){
              currentQuestion.assessment = assessment;
          }
        });
        save(true);
    }

    // Remove existing Encuesta
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.encuesta.$remove($state.go('encuestas.list'));
      }
    }

    function addQuestion(newQuestion) {

      var buildQuestion = function(question){
        var temp = question;
        if(temp.indexOf("?") === -1){
            temp = temp+"?";
        }
        if(temp.indexOf("¿") === -1){
            temp = "¿"+temp;
        }
        return temp;
      };

      if(newQuestion){
        var questionBuilt = buildQuestion(newQuestion);
        if(vm.encuesta.questions.indexOf(questionBuilt) === -1){
            vm.encuesta.questions.push({"question" : questionBuilt, "assessment" : 0});
        }else{
            vm.error = "La pregunta ya existe";
        }
      }
      vm.newQuestion = null;
    }

    // Save Encuesta
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.encuestaForm');
        return false;
      }
      //Se almacena el grupo al que le pertenece la encuesta
      vm.encuesta.group = $scope.groupSelected;

      // TODO: move create/update logic to service
      if (vm.encuesta._id) {
        vm.encuesta.$update(successCallback, errorCallback);
      } else {
        vm.encuesta.$save(successCallback, errorCallback);
      }

      function successCallback(res) {
        $state.go('encuestas.view', {
          encuestaId: res._id
        });
      }

      function errorCallback(res) {
        vm.error = res.data.message;
      }
    }

      function getGroup(thisGroupId, GrupoService) {
          return GrupoService.get({
              grupoId: thisGroupId
          });
      }
  }
}());
