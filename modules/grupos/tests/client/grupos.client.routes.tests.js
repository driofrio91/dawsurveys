(function () {
  'use strict';

  describe('Grupos Route Tests', function () {
    // Initialize global variables
    var $scope,
      GruposService;

    // We can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($rootScope, _GruposService_) {
      // Set a new global scope
      $scope = $rootScope.$new();
      GruposService = _GruposService_;
    }));

    describe('Route Config', function () {
      describe('Main Route', function () {
        var mainstate;
        beforeEach(inject(function ($state) {
          mainstate = $state.get('grupos');
        }));

        it('Should have the correct URL', function () {
          expect(mainstate.url).toEqual('/grupos');
        });

        it('Should be abstract', function () {
          expect(mainstate.abstract).toBe(true);
        });

        it('Should have template', function () {
          expect(mainstate.template).toBe('<ui-view/>');
        });
      });

      describe('View Route', function () {
        var viewstate,
          GruposController,
          mockGrupo;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          viewstate = $state.get('grupos.view');
          $templateCache.put('modules/grupos/client/views/view-grupo.client.view.html', '');

          // create mock Grupo
          mockGrupo = new GruposService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Grupo Name'
          });

          // Initialize Controller
          GruposController = $controller('GruposController as vm', {
            $scope: $scope,
            grupoResolve: mockGrupo
          });
        }));

        it('Should have the correct URL', function () {
          expect(viewstate.url).toEqual('/:grupoId');
        });

        it('Should have a resolve function', function () {
          expect(typeof viewstate.resolve).toEqual('object');
          expect(typeof viewstate.resolve.grupoResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(viewstate, {
            grupoId: 1
          })).toEqual('/grupos/1');
        }));

        it('should attach an Grupo to the controller scope', function () {
          expect($scope.vm.grupo._id).toBe(mockGrupo._id);
        });

        it('Should not be abstract', function () {
          expect(viewstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(viewstate.templateUrl).toBe('modules/grupos/client/views/view-grupo.client.view.html');
        });
      });

      describe('Create Route', function () {
        var createstate,
          GruposController,
          mockGrupo;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          createstate = $state.get('grupos.create');
          $templateCache.put('modules/grupos/client/views/form-grupo.client.view.html', '');

          // create mock Grupo
          mockGrupo = new GruposService();

          // Initialize Controller
          GruposController = $controller('GruposController as vm', {
            $scope: $scope,
            grupoResolve: mockGrupo
          });
        }));

        it('Should have the correct URL', function () {
          expect(createstate.url).toEqual('/create');
        });

        it('Should have a resolve function', function () {
          expect(typeof createstate.resolve).toEqual('object');
          expect(typeof createstate.resolve.grupoResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(createstate)).toEqual('/grupos/create');
        }));

        it('should attach an Grupo to the controller scope', function () {
          expect($scope.vm.grupo._id).toBe(mockGrupo._id);
          expect($scope.vm.grupo._id).toBe(undefined);
        });

        it('Should not be abstract', function () {
          expect(createstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(createstate.templateUrl).toBe('modules/grupos/client/views/form-grupo.client.view.html');
        });
      });

      describe('Edit Route', function () {
        var editstate,
          GruposController,
          mockGrupo;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          editstate = $state.get('grupos.edit');
          $templateCache.put('modules/grupos/client/views/form-grupo.client.view.html', '');

          // create mock Grupo
          mockGrupo = new GruposService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Grupo Name'
          });

          // Initialize Controller
          GruposController = $controller('GruposController as vm', {
            $scope: $scope,
            grupoResolve: mockGrupo
          });
        }));

        it('Should have the correct URL', function () {
          expect(editstate.url).toEqual('/:grupoId/edit');
        });

        it('Should have a resolve function', function () {
          expect(typeof editstate.resolve).toEqual('object');
          expect(typeof editstate.resolve.grupoResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(editstate, {
            grupoId: 1
          })).toEqual('/grupos/1/edit');
        }));

        it('should attach an Grupo to the controller scope', function () {
          expect($scope.vm.grupo._id).toBe(mockGrupo._id);
        });

        it('Should not be abstract', function () {
          expect(editstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(editstate.templateUrl).toBe('modules/grupos/client/views/form-grupo.client.view.html');
        });

        xit('Should go to unauthorized route', function () {

        });
      });

    });
  });
}());
