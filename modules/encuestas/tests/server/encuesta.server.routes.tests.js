'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Encuesta = mongoose.model('Encuesta'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app,
  agent,
  credentials,
  user,
  encuesta;

/**
 * Encuesta routes tests
 */
describe('Encuesta CRUD tests', function () {

  before(function (done) {
    // Get application
    app = express.init(mongoose);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      username: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: credentials.username,
      password: credentials.password,
      provider: 'local'
    });

    // Save a user to the test db and create new Encuesta
    user.save(function () {
      encuesta = {
        name: 'Encuesta name'
      };

      done();
    });
  });

  it('should be able to save a Encuesta if logged in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Encuesta
        agent.post('/api/encuestas')
          .send(encuesta)
          .expect(200)
          .end(function (encuestaSaveErr, encuestaSaveRes) {
            // Handle Encuesta save error
            if (encuestaSaveErr) {
              return done(encuestaSaveErr);
            }

            // Get a list of Encuestas
            agent.get('/api/encuestas')
              .end(function (encuestasGetErr, encuestasGetRes) {
                // Handle Encuestas save error
                if (encuestasGetErr) {
                  return done(encuestasGetErr);
                }

                // Get Encuestas list
                var encuestas = encuestasGetRes.body;

                // Set assertions
                (encuestas[0].user._id).should.equal(userId);
                (encuestas[0].name).should.match('Encuesta name');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to save an Encuesta if not logged in', function (done) {
    agent.post('/api/encuestas')
      .send(encuesta)
      .expect(403)
      .end(function (encuestaSaveErr, encuestaSaveRes) {
        // Call the assertion callback
        done(encuestaSaveErr);
      });
  });

  it('should not be able to save an Encuesta if no name is provided', function (done) {
    // Invalidate name field
    encuesta.name = '';

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Encuesta
        agent.post('/api/encuestas')
          .send(encuesta)
          .expect(400)
          .end(function (encuestaSaveErr, encuestaSaveRes) {
            // Set message assertion
            (encuestaSaveRes.body.message).should.match('Please fill Encuesta name');

            // Handle Encuesta save error
            done(encuestaSaveErr);
          });
      });
  });

  it('should be able to update an Encuesta if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Encuesta
        agent.post('/api/encuestas')
          .send(encuesta)
          .expect(200)
          .end(function (encuestaSaveErr, encuestaSaveRes) {
            // Handle Encuesta save error
            if (encuestaSaveErr) {
              return done(encuestaSaveErr);
            }

            // Update Encuesta name
            encuesta.name = 'WHY YOU GOTTA BE SO MEAN?';

            // Update an existing Encuesta
            agent.put('/api/encuestas/' + encuestaSaveRes.body._id)
              .send(encuesta)
              .expect(200)
              .end(function (encuestaUpdateErr, encuestaUpdateRes) {
                // Handle Encuesta update error
                if (encuestaUpdateErr) {
                  return done(encuestaUpdateErr);
                }

                // Set assertions
                (encuestaUpdateRes.body._id).should.equal(encuestaSaveRes.body._id);
                (encuestaUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should be able to get a list of Encuestas if not signed in', function (done) {
    // Create new Encuesta model instance
    var encuestaObj = new Encuesta(encuesta);

    // Save the encuesta
    encuestaObj.save(function () {
      // Request Encuestas
      request(app).get('/api/encuestas')
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Array).and.have.lengthOf(1);

          // Call the assertion callback
          done();
        });

    });
  });

  it('should be able to get a single Encuesta if not signed in', function (done) {
    // Create new Encuesta model instance
    var encuestaObj = new Encuesta(encuesta);

    // Save the Encuesta
    encuestaObj.save(function () {
      request(app).get('/api/encuestas/' + encuestaObj._id)
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Object).and.have.property('name', encuesta.name);

          // Call the assertion callback
          done();
        });
    });
  });

  it('should return proper error for single Encuesta with an invalid Id, if not signed in', function (done) {
    // test is not a valid mongoose Id
    request(app).get('/api/encuestas/test')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'Encuesta is invalid');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single Encuesta which doesnt exist, if not signed in', function (done) {
    // This is a valid mongoose Id but a non-existent Encuesta
    request(app).get('/api/encuestas/559e9cd815f80b4c256a8f41')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'No Encuesta with that identifier has been found');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to delete an Encuesta if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Encuesta
        agent.post('/api/encuestas')
          .send(encuesta)
          .expect(200)
          .end(function (encuestaSaveErr, encuestaSaveRes) {
            // Handle Encuesta save error
            if (encuestaSaveErr) {
              return done(encuestaSaveErr);
            }

            // Delete an existing Encuesta
            agent.delete('/api/encuestas/' + encuestaSaveRes.body._id)
              .send(encuesta)
              .expect(200)
              .end(function (encuestaDeleteErr, encuestaDeleteRes) {
                // Handle encuesta error error
                if (encuestaDeleteErr) {
                  return done(encuestaDeleteErr);
                }

                // Set assertions
                (encuestaDeleteRes.body._id).should.equal(encuestaSaveRes.body._id);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to delete an Encuesta if not signed in', function (done) {
    // Set Encuesta user
    encuesta.user = user;

    // Create new Encuesta model instance
    var encuestaObj = new Encuesta(encuesta);

    // Save the Encuesta
    encuestaObj.save(function () {
      // Try deleting Encuesta
      request(app).delete('/api/encuestas/' + encuestaObj._id)
        .expect(403)
        .end(function (encuestaDeleteErr, encuestaDeleteRes) {
          // Set message assertion
          (encuestaDeleteRes.body.message).should.match('User is not authorized');

          // Handle Encuesta error error
          done(encuestaDeleteErr);
        });

    });
  });

  it('should be able to get a single Encuesta that has an orphaned user reference', function (done) {
    // Create orphan user creds
    var _creds = {
      username: 'orphan',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create orphan user
    var _orphan = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'orphan@test.com',
      username: _creds.username,
      password: _creds.password,
      provider: 'local'
    });

    _orphan.save(function (err, orphan) {
      // Handle save error
      if (err) {
        return done(err);
      }

      agent.post('/api/auth/signin')
        .send(_creds)
        .expect(200)
        .end(function (signinErr, signinRes) {
          // Handle signin error
          if (signinErr) {
            return done(signinErr);
          }

          // Get the userId
          var orphanId = orphan._id;

          // Save a new Encuesta
          agent.post('/api/encuestas')
            .send(encuesta)
            .expect(200)
            .end(function (encuestaSaveErr, encuestaSaveRes) {
              // Handle Encuesta save error
              if (encuestaSaveErr) {
                return done(encuestaSaveErr);
              }

              // Set assertions on new Encuesta
              (encuestaSaveRes.body.name).should.equal(encuesta.name);
              should.exist(encuestaSaveRes.body.user);
              should.equal(encuestaSaveRes.body.user._id, orphanId);

              // force the Encuesta to have an orphaned user reference
              orphan.remove(function () {
                // now signin with valid user
                agent.post('/api/auth/signin')
                  .send(credentials)
                  .expect(200)
                  .end(function (err, res) {
                    // Handle signin error
                    if (err) {
                      return done(err);
                    }

                    // Get the Encuesta
                    agent.get('/api/encuestas/' + encuestaSaveRes.body._id)
                      .expect(200)
                      .end(function (encuestaInfoErr, encuestaInfoRes) {
                        // Handle Encuesta error
                        if (encuestaInfoErr) {
                          return done(encuestaInfoErr);
                        }

                        // Set assertions
                        (encuestaInfoRes.body._id).should.equal(encuestaSaveRes.body._id);
                        (encuestaInfoRes.body.name).should.equal(encuesta.name);
                        should.equal(encuestaInfoRes.body.user, undefined);

                        // Call the assertion callback
                        done();
                      });
                  });
              });
            });
        });
    });
  });

  afterEach(function (done) {
    User.remove().exec(function () {
      Encuesta.remove().exec(done);
    });
  });
});
