(function () {
  'use strict';

  angular
    .module('encuestas')
    .run(menuConfig);

  menuConfig.$inject = ['Menus', 'Authentication'];

  function menuConfig(menuService, authentication) {
    // Set top bar menu items
      if(authentication && authentication.user){
          menuService.addMenuItem('topbar', {
              title: 'Encuestas',
              state: 'encuestas',
              type: 'dropdown',
              roles: ['*']
          });

          // Add the dropdown list item
          menuService.addSubMenuItem('topbar', 'encuestas', {
              title: 'List Encuestas',
              state: 'encuestas.list'
          });
      }

    // /*Esta opcion solo estara disponible para los admins*/
    if(authentication && authentication.user && authentication.user.roles.indexOf("admin") !== -1){
        // Add the dropdown create item
        menuService.addSubMenuItem('topbar', 'encuestas', {
            title: 'Create Encuesta',
            state: 'encuestas.create',
            roles: ['user']
        });
    }
  }
}());
