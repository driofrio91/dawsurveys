'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Grupo Schema
 */
var GrupoSchema = new Schema({
  name: {
    type: String,
    default: '',
    required: 'Please fill Grupo name',
    trim: true
  },
  created: {
    type: Date,
    default: Date.now
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
    groupUsers : {
        type : Array,
        default : []
    }
});

mongoose.model('Grupo', GrupoSchema);
