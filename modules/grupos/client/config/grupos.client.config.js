(function () {
  'use strict';

  angular
    .module('grupos')
    .run(menuConfig);

  menuConfig.$inject = ['Menus', 'Authentication'];

  function menuConfig(menuService, authentication) {
    // Set top bar menu items
      if(authentication && authentication.user.roles
          && authentication.user.roles.indexOf("admin") !== -1){
          menuService.addMenuItem('topbar', {
              title: 'Grupos',
              state: 'grupos',
              type: 'dropdown',
              roles: ['*']
          });

          // Add the dropdown list item
          menuService.addSubMenuItem('topbar', 'grupos', {
              title: 'Lista de Grupos',
              state: 'grupos.list'
          });
      }

    /*Esta opcion solo estara disponible para los admins*/
    if(authentication && authentication.user && authentication.user.roles.indexOf("admin") !== -1){
        // Add the dropdown create item
        menuService.addSubMenuItem('topbar', 'grupos', {
            title: 'Crear Grupo',
            state: 'grupos.create',
            roles: ['user']
        });
    }
  }
}());
