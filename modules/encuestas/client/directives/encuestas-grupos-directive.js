/**
 * Created by Danny on 05/12/2016.
 */
'use strict';

angular.module('encuestas').directive('listFormGroups', function() {
    return {
        templateUrl : "modules/encuestas/client/views/list-grupos.client.view.html",
        controller : "GruposListControllerEncuestas"
    };
})
    .directive('listViewGroup', function() {
        return {
            templateUrl : "modules/encuestas/client/views/view-group.client.view.html",
            controller : "GruposListControllerEncuestas",
        };
    })
;
