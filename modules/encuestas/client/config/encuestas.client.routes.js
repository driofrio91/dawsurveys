(function () {
  'use strict';

  angular
    .module('encuestas')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('encuestas', {
        abstract: true,
        url: '/encuestas',
        template: '<ui-view/>'
      })
      .state('encuestas.list', {
        url: '',
        templateUrl: 'modules/encuestas/client/views/list-encuestas.client.view.html',
        controller: 'EncuestasListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Encuestas List'
        }
      })
      .state('encuestas.create', {
        url: '/create',
        templateUrl: 'modules/encuestas/client/views/form-encuesta.client.view.html',
        controller: 'EncuestasController',
        controllerAs: 'vm',
        resolve: {
          encuestaResolve: newEncuesta
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Encuestas Create'
        }
      })
      .state('encuestas.edit', {
        url: '/:encuestaId/edit',
        templateUrl: 'modules/encuestas/client/views/form-encuesta.client.view.html',
        controller: 'EncuestasController',
        controllerAs: 'vm',
        resolve: {
          encuestaResolve: getEncuesta
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Edit Encuesta {{ encuestaResolve.name }}'
        }
      })
      .state('encuestas.view', {
        url: '/:encuestaId',
        templateUrl: 'modules/encuestas/client/views/view-encuesta.client.view.html',
        controller: 'EncuestasController',
        controllerAs: 'vm',
        resolve: {
          encuestaResolve: getEncuesta
        },
        data: {
          pageTitle: 'Encuesta {{ encuestaResolve.name }}'
        }
      });
  }

  getEncuesta.$inject = ['$stateParams', 'EncuestasService'];

  function getEncuesta($stateParams, EncuestasService) {
    return EncuestasService.get({
      encuestaId: $stateParams.encuestaId
    }).$promise;
  }

  newEncuesta.$inject = ['EncuestasService'];

  function newEncuesta(EncuestasService) {
    return new EncuestasService();
  }
}());
