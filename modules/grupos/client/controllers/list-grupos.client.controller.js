(function () {
  'use strict';

  angular
    .module('grupos')
    .controller('GruposListController', GruposListController);

  GruposListController.$inject = ['GruposService'];

  function GruposListController(GruposService) {
    var vm = this;

    vm.grupos = GruposService.query(function (data) {
        console.log("---> "+data);
    });
  }
}());
