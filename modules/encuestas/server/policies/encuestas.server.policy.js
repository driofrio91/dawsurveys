'use strict';

/**
 * Module dependencies
 */
var acl = require('acl');

// Using the memory backend
acl = new acl(new acl.memoryBackend());

/**
 * Invoke Encuestas Permissions
 */
exports.invokeRolesPolicies = function () {
  acl.allow([{
    roles: ['admin'],
    allows: [{
      resources: '/api/encuestas',
      permissions: '*'
    }, {
      resources: '/api/encuestas/:encuestaId',
      permissions: '*'
    }]
  }, {
    roles: ['user'],
    allows: [{
      resources: '/api/encuestas',
      permissions: ['get', 'post']
    }, {
      resources: '/api/encuestas/:encuestaId',
      permissions: ['get']
    }]
  }, {
    roles: ['guest'],
    allows: [{
      resources: '/api/encuestas',
      permissions: ['get']
    }, {
      resources: '/api/encuestas/:encuestaId',
      permissions: ['get']
    }]
  }]);
};

/**
 * Check If Encuestas Policy Allows
 */
exports.isAllowed = function (req, res, next) {
  var roles = (req.user) ? req.user.roles : ['guest'];

  // If an Encuesta is being processed and the current user created it then allow any manipulation
  // if (req.encuesta && req.user && req.encuesta.user && req.encuesta.user.id === req.user.id) {
  //   return next();
  // }

  // Check for user roles
  acl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), function (err, isAllowed) {
    if (err) {
      // An authorization error occurred
      return res.status(500).send('Unexpected authorization error');
    } else {
      // if (isAllowed) {
        // Access granted! Invoke next middleware
        return next();
      // } else {
      //   return res.status(403).json({
      //     message: 'User is not authorized'
      //   });
      // }
    }
  });
};
