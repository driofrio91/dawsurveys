'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Encuesta Schema
 */
var EncuestaSchema = new Schema({
  name: {
    type: String,
    default: '',
    required: 'Por favor rellena el nombre de la Encuesta',
    trim: true
  },
  created: {
    type: Date,
    default: Date.now
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
    questions : {
        type : Array,
        required: 'Se neceita un minimo de 1 preguntas para la encuesta',
        default : []
    },
    group: {
        type: Schema.ObjectId,
        ref: 'Group'
    }
});

mongoose.model('Encuesta', EncuestaSchema);
