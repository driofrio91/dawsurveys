(function () {
  'use strict';

  angular
    .module('encuestas')
    .controller('EncuestasListController', EncuestasListController);

  EncuestasListController.$inject = ['EncuestasService', 'Authentication', 'GrupoService'];

  function EncuestasListController(EncuestasService, Authentication, GrupoService) {
    var vm = this;
    vm.encuestas = [];

    EncuestasService.query(function (encuestas) {
        if(Authentication.user.roles.indexOf("admin") === -1){
            var grupos = GrupoService.query();
            grupos.$promise.then(function (data) {
                var finalData = [];
                data.forEach(function (currentData) {
                    if(currentData.groupUsers.indexOf(Authentication.user._id) !== -1){
                        finalData.push(currentData);
                    }
                });
                encuestas.forEach(function (current) {
                    finalData.forEach(function (currentGroup) {
                        if(current.group === currentGroup._id){
                            vm.encuestas.push(current);
                        }
                    });
                });
            });
        }else{
            vm.encuestas = encuestas;
        }
    });
  }
}());
