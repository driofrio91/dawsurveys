'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Encuesta = mongoose.model('Encuesta'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Create a Encuesta
 */
exports.create = function(req, res) {
  var encuesta = new Encuesta(req.body);
  encuesta.user = req.user;

  encuesta.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(encuesta);
    }
  });
};

/**
 * Show the current Encuesta
 */
exports.read = function(req, res) {
  // convert mongoose document to JSON
  var encuesta = req.encuesta ? req.encuesta.toJSON() : {};

  // Add a custom field to the Article, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  encuesta.isCurrentUserOwner = req.user && encuesta.user && encuesta.user._id.toString() === req.user._id.toString();

  res.jsonp(encuesta);
};

/**
 * Update a Encuesta
 */
exports.update = function(req, res) {
  var encuesta = req.encuesta;

  encuesta = _.extend(encuesta, req.body);

  encuesta.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(encuesta);
    }
  });
};

/**
 * Delete an Encuesta
 */
exports.delete = function(req, res) {
  var encuesta = req.encuesta;

  encuesta.remove(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(encuesta);
    }
  });
};

/**
 * List of Encuestas
 */
exports.list = function(req, res) {
  Encuesta.find().sort('-created').populate('user', 'displayName').exec(function(err, encuestas) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(encuestas);
    }
  });
};

/**
 * Encuesta middleware
 */
exports.encuestaByID = function(req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Encuesta is invalid'
    });
  }

  Encuesta.findById(id).populate('user', 'displayName').exec(function (err, encuesta) {
    if (err) {
      return next(err);
    } else if (!encuesta) {
      return res.status(404).send({
        message: 'No Encuesta with that identifier has been found'
      });
    }
    req.encuesta = encuesta;
    next();
  });
};
