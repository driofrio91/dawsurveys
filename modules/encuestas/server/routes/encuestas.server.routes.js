'use strict';

/**
 * Module dependencies
 */
var encuestasPolicy = require('../policies/encuestas.server.policy'),
  encuestas = require('../controllers/encuestas.server.controller');

module.exports = function(app) {
  // Encuestas Routes
  app.route('/api/encuestas').all(encuestasPolicy.isAllowed)
    .get(encuestas.list)
    .post(encuestas.create);

  app.route('/api/encuestas/:encuestaId').all(encuestasPolicy.isAllowed)
    .get(encuestas.read)
    .put(encuestas.update)
    .delete(encuestas.delete);

  // Finish by binding the Encuesta middleware
  app.param('encuestaId', encuestas.encuestaByID);
};
