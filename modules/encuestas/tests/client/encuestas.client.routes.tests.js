(function () {
  'use strict';

  describe('Encuestas Route Tests', function () {
    // Initialize global variables
    var $scope,
      EncuestasService;

    // We can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($rootScope, _EncuestasService_) {
      // Set a new global scope
      $scope = $rootScope.$new();
      EncuestasService = _EncuestasService_;
    }));

    describe('Route Config', function () {
      describe('Main Route', function () {
        var mainstate;
        beforeEach(inject(function ($state) {
          mainstate = $state.get('encuestas');
        }));

        it('Should have the correct URL', function () {
          expect(mainstate.url).toEqual('/encuestas');
        });

        it('Should be abstract', function () {
          expect(mainstate.abstract).toBe(true);
        });

        it('Should have template', function () {
          expect(mainstate.template).toBe('<ui-view/>');
        });
      });

      describe('View Route', function () {
        var viewstate,
          EncuestasController,
          mockEncuesta;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          viewstate = $state.get('encuestas.view');
          $templateCache.put('modules/encuestas/client/views/view-encuesta.client.view.html', '');

          // create mock Encuesta
          mockEncuesta = new EncuestasService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Encuesta Name'
          });

          // Initialize Controller
          EncuestasController = $controller('EncuestasController as vm', {
            $scope: $scope,
            encuestaResolve: mockEncuesta
          });
        }));

        it('Should have the correct URL', function () {
          expect(viewstate.url).toEqual('/:encuestaId');
        });

        it('Should have a resolve function', function () {
          expect(typeof viewstate.resolve).toEqual('object');
          expect(typeof viewstate.resolve.encuestaResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(viewstate, {
            encuestaId: 1
          })).toEqual('/encuestas/1');
        }));

        it('should attach an Encuesta to the controller scope', function () {
          expect($scope.vm.encuesta._id).toBe(mockEncuesta._id);
        });

        it('Should not be abstract', function () {
          expect(viewstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(viewstate.templateUrl).toBe('modules/encuestas/client/views/view-encuesta.client.view.html');
        });
      });

      describe('Create Route', function () {
        var createstate,
          EncuestasController,
          mockEncuesta;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          createstate = $state.get('encuestas.create');
          $templateCache.put('modules/encuestas/client/views/form-encuesta.client.view.html', '');

          // create mock Encuesta
          mockEncuesta = new EncuestasService();

          // Initialize Controller
          EncuestasController = $controller('EncuestasController as vm', {
            $scope: $scope,
            encuestaResolve: mockEncuesta
          });
        }));

        it('Should have the correct URL', function () {
          expect(createstate.url).toEqual('/create');
        });

        it('Should have a resolve function', function () {
          expect(typeof createstate.resolve).toEqual('object');
          expect(typeof createstate.resolve.encuestaResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(createstate)).toEqual('/encuestas/create');
        }));

        it('should attach an Encuesta to the controller scope', function () {
          expect($scope.vm.encuesta._id).toBe(mockEncuesta._id);
          expect($scope.vm.encuesta._id).toBe(undefined);
        });

        it('Should not be abstract', function () {
          expect(createstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(createstate.templateUrl).toBe('modules/encuestas/client/views/form-encuesta.client.view.html');
        });
      });

      describe('Edit Route', function () {
        var editstate,
          EncuestasController,
          mockEncuesta;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          editstate = $state.get('encuestas.edit');
          $templateCache.put('modules/encuestas/client/views/form-encuesta.client.view.html', '');

          // create mock Encuesta
          mockEncuesta = new EncuestasService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Encuesta Name'
          });

          // Initialize Controller
          EncuestasController = $controller('EncuestasController as vm', {
            $scope: $scope,
            encuestaResolve: mockEncuesta
          });
        }));

        it('Should have the correct URL', function () {
          expect(editstate.url).toEqual('/:encuestaId/edit');
        });

        it('Should have a resolve function', function () {
          expect(typeof editstate.resolve).toEqual('object');
          expect(typeof editstate.resolve.encuestaResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(editstate, {
            encuestaId: 1
          })).toEqual('/encuestas/1/edit');
        }));

        it('should attach an Encuesta to the controller scope', function () {
          expect($scope.vm.encuesta._id).toBe(mockEncuesta._id);
        });

        it('Should not be abstract', function () {
          expect(editstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(editstate.templateUrl).toBe('modules/encuestas/client/views/form-encuesta.client.view.html');
        });

        xit('Should go to unauthorized route', function () {

        });
      });

    });
  });
}());
