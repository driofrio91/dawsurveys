/**
 * Created by Danny on 05/12/2016.
 */
'use strict';

angular.module('grupos').directive('listFormUser', function() {
    return {
        templateUrl : "modules/grupos/client/views/form-list-users.client.view.html",
        controller : "UserListGroupController"
    };
})
    .directive('listViewUser', function() {
        return {
            templateUrl : "modules/grupos/client/views/view-list-users.client.view.html",
            controller : "UserListGroupController",
            /*scope : {
                userList : "=userList"
            }*/
        };
    });
